import unittest
from trashsmash.modules import config_module


class SetupConfigTestCase(unittest.TestCase):
    config_list = {
        'name': 'default',
        'list': {
            'default': "default config",
            'config': "another config",
        }
    }

    def test_change_default_config(self):
        self.assertEqual(config_module.setup_config(self.config_list, 'default'), 2)

    def test_change_wrong_config(self):
        self.assertEqual(config_module.setup_config(self.config_list, 'wrong'), 1)


class CheckoutConfigTestCase(unittest.TestCase):
    config_list = None

    def setUp(self):
        self.config_list = {
            'name': 'default',
            'list': {
                'default': "default config",
                'config': "another config",
            }
        }

    def test_checkout_config(self):
        new_config = 'config'
        self.assertEqual(config_module.checkout_config(self.config_list, new_config), 0)
        self.assertEqual(self.config_list['name'], new_config)

    def test_checkout_wrong_config(self):
        self.assertEqual(config_module.checkout_config(self.config_list, 'wrong'), 1)
        self.assertEqual(self.config_list['name'], 'default')


class CreateConfigTestCase(unittest.TestCase):
    config_list = None

    def setUp(self):
        self.config_list = {
            'name': 'default',
            'list': {
                'default': "default config",
                'config': "another config",
            }
        }

    def test_create_config(self):
        new_config = 'test'
        self.assertEqual(config_module.create_config(self.config_list, new_config), 0)
        self.assertEqual(self.config_list['list']['default'], self.config_list['list'][new_config])

    def test_create_existing_config(self):
        self.assertEqual(config_module.create_config(self.config_list, 'config'), 1)


class DeleteConfigTestCase(unittest.TestCase):
    config_list = None

    def setUp(self):
        self.config_list = {
            'name': 'default',
            'list': {
                'default': "default config",
                'config': "another config",
            }
        }

    def test_delete_config(self):
        self.assertEqual(config_module.delete_config(self.config_list, 'config'), 0)
        self.assertFalse('config' in self.config_list['list'])

    def test_delete_current_config(self):
        self.config_list['name'] = 'config'
        self.assertEqual(config_module.delete_config(self.config_list, 'config'), 0)
        self.assertFalse('config' in self.config_list['list'])
        self.assertEqual(self.config_list['name'], 'default')

    def test_delete_wrong_config(self):
        self.assertEqual(config_module.delete_config(self.config_list, 'wrong'), 1)

    def test_delete_default_config(self):
        self.assertEqual(config_module.delete_config(self.config_list, 'default'), 2)
        self.assertTrue('default' in self.config_list['list'])


def suite():
    result = unittest.TestSuite()
    result.addTest(SetupConfigTestCase('test_change_default_config'))
    result.addTest(SetupConfigTestCase('test_change_wrong_config'))
    result.addTest(CheckoutConfigTestCase('test_checkout_config'))
    result.addTest(CheckoutConfigTestCase('test_checkout_wrong_config'))
    result.addTest(CreateConfigTestCase('test_create_config'))
    result.addTest(CreateConfigTestCase('test_create_existing_config'))
    result.addTest(DeleteConfigTestCase('test_delete_config'))
    result.addTest(DeleteConfigTestCase('test_delete_current_config'))
    result.addTest(DeleteConfigTestCase('test_delete_wrong_config'))
    result.addTest(DeleteConfigTestCase('test_delete_default_config'))
    return result
