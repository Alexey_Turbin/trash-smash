from os.path import (
    dirname,
    abspath,
    join,
    exists,
    basename,
)

from os import (
    remove,
    mkfifo,
    mkdir,
    chmod,
    listdir,
)

from shutil import rmtree
from trashsmash.modules import filelist_module
import unittest


class ShowFilesTestCase(unittest.TestCase):
    list_of_files_in_trash = {
        'file1': {
            'old_name': '/some/path/file1',
            'date': 'some_date',
        },
        'file2': {
            'old_name': '/some/path/file2',
            'date': 'some_date',
        }

    }

    def test_print_full_exit_code(self):
        list_before = self.list_of_files_in_trash
        self.assertEqual(filelist_module.show_files(self.list_of_files_in_trash, full_info=True), 0)
        self.assertEqual(list_before, self.list_of_files_in_trash)

    def test_print_short_exit_code(self):
        list_before = self.list_of_files_in_trash
        self.assertEqual(filelist_module.show_files(self.list_of_files_in_trash, full_info=False), 0)
        self.assertEqual(list_before, self.list_of_files_in_trash)


def suite():
    result = unittest.TestSuite()
    result.addTest(ShowFilesTestCase('test_print_full_exit_code'))
    result.addTest(ShowFilesTestCase('test_print_short_exit_code'))
    return result