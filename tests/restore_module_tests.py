import unittest
from os import (
    mkfifo,
    mkdir,
    chmod,
)
from os.path import (
    dirname,
    abspath,
    join,
    exists,
    basename,
)
from shutil import rmtree

from trashsmash.modules import restore_module


class RestoreFilesTestCase(unittest.TestCase):
    trash_folder = join(dirname(abspath(__file__)), 'TestDir', 'trash')
    test_folder = join(dirname(abspath(__file__)), 'TestDir', 'test')
    first_file_name = 'file1'
    second_file_name = 'file2'

    def setUp(self):
        mkdir(self.trash_folder)
        mkfifo(join(self.trash_folder, self.first_file_name))
        mkfifo(join(self.trash_folder, self.second_file_name))

        mkdir(self.test_folder)

    def tearDown(self):
        rmtree(self.trash_folder)
        rmtree(self.test_folder)

    def runTest(self):
        first_file_old_path = join(self.test_folder, 'file')
        second_file_old_path = join(self.test_folder, 'inner', 'file')
        list_of_files_in_trash = {
            self.first_file_name: {
                'old_name': first_file_old_path,
            },
            self.second_file_name: {
                'old_name': second_file_old_path,
            }
        }

        self.assertEqual(
            restore_module.restore_files(
                [self.first_file_name, self.second_file_name],
                self.trash_folder,
                list_of_files_in_trash,
                dry_run=False,
                ask_confirmation=False),
            0)

        self.assertFalse(exists(join(self.trash_folder, self.first_file_name)))
        self.assertFalse(exists(join(self.trash_folder, self.second_file_name)))

        self.assertTrue(exists(first_file_old_path))
        self.assertTrue(exists(second_file_old_path))

        self.assertEqual(list_of_files_in_trash.__len__(), 0)


class RestoreFilesErrorsTestCase(unittest.TestCase):
    trash_folder = join(dirname(abspath(__file__)), 'TestDir', 'trash')
    test_folder = join(dirname(abspath(__file__)), 'TestDir', 'test')
    inner_test_folder = join(test_folder, 'inner')
    file_to_restore = join(trash_folder, 'file_to_restore')
    file_to_restore_name = basename(file_to_restore)
    file_in_test_folder = join(test_folder, 'file')

    def setUp(self):
        mkdir(self.trash_folder)
        mkfifo(self.file_to_restore)

        mkdir(self.test_folder)
        mkdir(self.file_in_test_folder)
        mkdir(self.inner_test_folder)

        chmod(self.inner_test_folder, 0)

    def tearDown(self):
        rmtree(self.trash_folder)
        chmod(self.inner_test_folder, 511)
        rmtree(self.test_folder)

    def test_same_name_error(self):
        file_old_name = join(self.test_folder, 'file')
        list_of_files_in_trash = {
            self.file_to_restore_name: {
                'old_name': file_old_name
            }
        }

        self.assertEqual(
            restore_module.restore_files(
                [self.file_to_restore_name],
                self.trash_folder,
                list_of_files_in_trash,
            ),
            4)

        self.assertTrue(exists(self.file_to_restore))

    def test_file_doesnt_exist_error(self):
        ghost_file_name = 'ghost'
        file_old_name = join(self.test_folder, 'ghost')
        list_of_files_in_trash = {
            ghost_file_name: {
                'old_name': file_old_name
            }
        }

        self.assertEqual(
            restore_module.restore_files(
                [ghost_file_name],
                self.trash_folder,
                list_of_files_in_trash,
            ),
            2)

    def test_file_has_bad_old_name_error(self):
        file_old_name = 'bad_name'
        list_of_files_in_trash = {
            self.file_to_restore_name: {
                'old_name': file_old_name
            }
        }

        self.assertEqual(
            restore_module.restore_files(
                [self.file_to_restore_name],
                self.trash_folder,
                list_of_files_in_trash,
            ),
            5)

    def test_file_not_in_list_error(self):
        list_of_files_in_trash = {}

        self.assertEqual(
                restore_module.restore_files(
                        [self.file_to_restore_name],
                        self.trash_folder,
                        list_of_files_in_trash,
                        ),
                6)

    def test_permission_denied_error(self):
        file_old_name = join(self.inner_test_folder, 'file')
        list_of_files_in_trash = {
            self.file_to_restore_name: {
                'old_name': file_old_name
            }
        }

        self.assertEqual(
                restore_module.restore_files(
                        [self.file_to_restore_name],
                        self.trash_folder,
                        list_of_files_in_trash,
                        ),
                3)


class DryRunRestoreFilesTestCase(unittest.TestCase):
    trash_folder = join(dirname(abspath(__file__)), 'TestDir', 'trash')
    test_folder = join(dirname(abspath(__file__)), 'TestDir', 'test')
    first_file_name = 'file1'
    second_file_name = 'file2'

    def setUp(self):
        mkdir(self.trash_folder)
        mkfifo(join(self.trash_folder, self.first_file_name))
        mkfifo(join(self.trash_folder, self.second_file_name))

        mkdir(self.test_folder)

    def tearDown(self):
        rmtree(self.trash_folder)
        rmtree(self.test_folder)

    def runTest(self):
        first_file_old_path = join(self.test_folder, 'file')
        second_file_old_path = join(self.test_folder, 'inner', 'file')
        list_of_files_in_trash = {
            self.first_file_name: {
                'old_name': first_file_old_path,
            },
            self.second_file_name: {
                'old_name': second_file_old_path,
            }
        }

        self.assertEqual(
            restore_module.restore_files(
                [self.first_file_name, self.second_file_name],
                self.trash_folder,
                list_of_files_in_trash,
                dry_run=True,
                ask_confirmation=False),
            0)

        self.assertTrue(exists(join(self.trash_folder, self.first_file_name)))
        self.assertTrue(exists(join(self.trash_folder, self.second_file_name)))

        self.assertFalse(exists(first_file_old_path))
        self.assertFalse(exists(second_file_old_path))

        self.assertEqual(list_of_files_in_trash.__len__(), 0)


def suite():
    result = unittest.TestSuite()
    result.addTest(RestoreFilesTestCase())
    result.addTest(RestoreFilesErrorsTestCase('test_same_name_error'))
    result.addTest(RestoreFilesErrorsTestCase('test_file_doesnt_exist_error'))
    result.addTest(RestoreFilesErrorsTestCase('test_file_has_bad_old_name_error'))
    result.addTest(RestoreFilesErrorsTestCase('test_file_not_in_list_error'))
    result.addTest(RestoreFilesErrorsTestCase('test_permission_denied_error'))
    result.addTest(DryRunRestoreFilesTestCase())
    return result
