from os.path import (
    dirname,
    abspath,
    join,
    exists,
    basename,
)

from os import (
    remove,
    mkfifo,
    mkdir,
    chmod,
    listdir,
)

from shutil import rmtree
from trashsmash.modules import remove_module
import unittest
import argparse


class SetNameTestCase(unittest.TestCase):
    file_path = join(dirname(abspath(__file__)), 'TestDir', 'file')

    def setUp(self):
        mkfifo(self.file_path)

    def tearDown(self):
        remove(self.file_path)
        pass

    def runTest(self):
        first_new_path = remove_module.set_new_path(self.file_path)
        self.assertNotEqual(first_new_path, self.file_path)

        second_new_path = remove_module.set_new_path(first_new_path)
        self.assertEqual(first_new_path, second_new_path)


class RemoveFileTestCase(unittest.TestCase):
    trash_folder = join(dirname(abspath(__file__)), 'TestDir', 'trash')
    first_test_folder = join(dirname(abspath(__file__)), 'TestDir', 'test1')
    second_test_folder = join(dirname(abspath(__file__)), 'TestDir', 'test2')
    first_file = join(first_test_folder, 'file')
    second_file = join(second_test_folder, 'file')

    def setUp(self):
        mkdir(self.trash_folder)

        mkdir(self.first_test_folder)
        with open(join(self.first_test_folder, self.first_file), 'w') as f:
            f.write(join(self.first_test_folder, self.first_file))

        mkdir(self.second_test_folder)
        with open(join(self.second_test_folder, self.second_file), 'w') as f:
            f.write(join(self.second_test_folder, self.second_file))

    def tearDown(self):
        rmtree(self.trash_folder)
        rmtree(self.first_test_folder)
        rmtree(self.second_test_folder)

    def runTest(self):
        list_of_files = {}

        remove_module.remove_file(self.first_file, self.trash_folder, list_of_files)

        self.assertFalse(exists(self.first_file))

        first_file_in_trash = listdir(self.trash_folder)[0]

        print"{}=-----------".format(list_of_files)
        with open(join(self.trash_folder, first_file_in_trash)) as f:
            self.assertEqual(f.read(), list_of_files[first_file_in_trash]['old_name'])

        remove_module.remove_file(self.second_file, self.trash_folder, list_of_files)

        self.assertFalse(exists(self.second_file))
        list = listdir(self.trash_folder)
        first_file_in_trash = list[0]
        second_file_in_trash = list[1]
        with open(join(self.trash_folder, first_file_in_trash)) as f:
            self.assertEqual(f.read(), list_of_files[first_file_in_trash]['old_name'])
        with open(join(self.trash_folder, second_file_in_trash)) as f:
            self.assertEqual(f.read(), list_of_files[second_file_in_trash]['old_name'])


class RemoveFilesTestCase(unittest.TestCase):
    trash_folder = join(dirname(abspath(__file__)), 'TestDir', 'trash')
    test_folder = join(dirname(abspath(__file__)), 'TestDir', 'test')
    files = [
        join(dirname(abspath(__file__)), 'TestDir', 'test', 'file1'),
        join(dirname(abspath(__file__)), 'TestDir', 'test', 'file2'),
        join(dirname(abspath(__file__)), 'TestDir', 'test', 'file3'),
        join(dirname(abspath(__file__)), 'TestDir', 'test', 'file4'),
        join(dirname(abspath(__file__)), 'TestDir', 'test', 'file5'),
    ]

    def setUp(self):
        mkdir(self.trash_folder)
        mkdir(self.test_folder)

        for item in self.files:
            mkfifo(item)

    def tearDown(self):
        rmtree(self.trash_folder)
        rmtree(self.test_folder)

    def runTest(self):
        list_of_files = {}

        remove_module.remove_files(self.files, self.trash_folder, list_of_files)

        self.assertEqual(listdir(self.test_folder).__len__(), 0)

        for item in listdir(self.trash_folder):
            self.assertTrue(item in list_of_files)


class RemoveFilesErrorsTestCase(unittest.TestCase):
    trash_folder = join(dirname(abspath(__file__)), 'TestDir', 'trash')
    test_folder = join(dirname(abspath(__file__)), 'TestDir', 'test')
    inner_test_folder = join(test_folder, 'inner')
    sys_file = join(trash_folder, 'sysfile')

    def setUp(self):
        mkdir(self.trash_folder)
        mkfifo(self.sys_file)

        mkdir(self.test_folder)
        mkdir(self.inner_test_folder)

        chmod(self.inner_test_folder, 0)

    def tearDown(self):
        rmtree(self.trash_folder)
        chmod(self.inner_test_folder, 511)
        rmtree(self.test_folder)

    def runTest(self):
        cant_reach_file = join(self.inner_test_folder, 'file')
        does_not_exist_file = join(self.test_folder, 'ghost')
        list_of_files = {}
        self.assertEqual(
                remove_module.remove_files(
                        [does_not_exist_file],
                        self.trash_folder,
                        list_of_files),
                2)
        self.assertEqual(
                remove_module.remove_files(
                        [cant_reach_file],
                        self.trash_folder,
                        list_of_files),
                3)
        self.assertEqual(
                remove_module.remove_files(
                        [self.sys_file],
                        self.trash_folder,
                        list_of_files,
                        sys_folders=[self.trash_folder]),
                4)

        self.assertEqual(
                remove_module.remove_files(
                        [cant_reach_file, does_not_exist_file, self.sys_file],
                        self.trash_folder,
                        list_of_files,
                        sys_folders=[self.trash_folder]),
                1)

        self.assertEqual(list_of_files.__len__(), 0)


class DryRunRemoveFilesTestCase(unittest.TestCase):
    trash_folder = join(dirname(abspath(__file__)), 'TestDir', 'trash')
    test_folder = join(dirname(abspath(__file__)), 'TestDir', 'test')
    inner_test_folder = join(test_folder, 'inner')
    file = join(test_folder, 'file')
    sys_file = join(trash_folder, 'sysfile')

    def setUp(self):
        mkdir(self.trash_folder)
        mkfifo(self.sys_file)

        mkdir(self.test_folder)
        mkfifo(self.file)
        mkdir(self.inner_test_folder)

        chmod(self.inner_test_folder, 0)

    def tearDown(self):
        rmtree(self.trash_folder)
        chmod(self.inner_test_folder, 511)
        rmtree(self.test_folder)

    def runTest(self):
        cant_reach_file = join(self.inner_test_folder, 'file')
        does_not_exist_file = join(self.test_folder, 'ghost')
        list_of_files = {}
        self.assertEqual(
                remove_module.remove_files(
                        [does_not_exist_file],
                        self.trash_folder,
                        list_of_files,
                        dry_run=True),
                2)
        self.assertEqual(
                remove_module.remove_files(
                        [cant_reach_file],
                        self.trash_folder,
                        list_of_files,
                        dry_run=True),
                3)
        self.assertEqual(
                remove_module.remove_files(
                        [self.sys_file],
                        self.trash_folder,
                        list_of_files,
                        sys_folders=[self.trash_folder],
                        dry_run=True),
                4)

        self.assertEqual(
                remove_module.remove_files(
                        [cant_reach_file, does_not_exist_file, self.sys_file],
                        self.trash_folder,
                        list_of_files,
                        sys_folders=[self.trash_folder],
                        dry_run=True),
                1)

        self.assertEqual(list_of_files.__len__(), 0)

        self.assertEqual(
            remove_module.remove_files(
                [self.file],
                self.trash_folder,
                list_of_files,
                sys_folders=[self.trash_folder],
                dry_run=True),
            0)

        self.assertTrue(exists(self.file))
        self.assertEqual(list_of_files.__len__(), 1)


def suite():
    result = unittest.TestSuite()
    result.addTest(SetNameTestCase())
    result.addTest(RemoveFileTestCase())
    result.addTest(RemoveFilesTestCase())
    result.addTest(RemoveFilesErrorsTestCase())
    result.addTest(DryRunRemoveFilesTestCase())
    return result

