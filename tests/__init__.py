import unittest
import remove_module_tests
import restore_module_tests
import filelist_module_tests
import config_module_tests
import empty_module_tests
import console_commands_tests


def suite():
    result = list()
    result.append(remove_module_tests.suite())
    result.append(restore_module_tests.suite())
    result.append(filelist_module_tests.suite())
    result.append(config_module_tests.suite())
    result.append(empty_module_tests.suite())
    result.append(console_commands_tests.suite())
    return unittest.TestSuite(result)


def run_all_test():
    all_tests = suite()
    unittest.TextTestRunner(verbosity=2).run(all_tests)