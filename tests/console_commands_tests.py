import subprocess
import unittest
from os import (
    mkfifo,
    mkdir,
)
from os.path import (
    dirname,
    abspath,
    join,
    exists,
)
from shutil import rmtree

from trashsmash.utils import (
    create_default_config_files,
    create_trash_folder,
)


class ConsoleCommandsTestCase(unittest.TestCase):
    json_config_old = ''
    txt_config_old = ''
    logs_old = ''
    program_location = join(dirname(dirname(abspath(__file__))))
    program_script = join(program_location, 'trash-smash.py')
    program_folder = join(program_location, 'trashsmash')
    program_txt_config = join(program_folder, 'config.txt')
    program_json_config = join(program_folder, 'config')
    program_logs = join(program_folder, 'logs.txt')
    trash_folder_location = join(dirname(abspath(__file__)), 'TestDir', 'trash')
    trash_folder_name = 'Trash'
    test_folder = join(dirname(abspath(__file__)), 'TestDir', 'test')
    file = join(test_folder, 'file')

    def setUp(self):
        with open(self.program_json_config, 'r') as f:
            self.json_config_old = f.read()
        with open(self.program_txt_config, 'r') as f:
            self.txt_config_old = f.read()
        with open(self.program_logs, 'r') as f:
            self.logs_old = f.read()
        mkdir(self.trash_folder_location)
        mkdir(self.test_folder)
        mkfifo(self.file)

    def tearDown(self):
        with open(self.program_json_config, 'w') as f:
            f.write(self.json_config_old)
        with open(self.program_txt_config, 'w') as f:
             f.write(self.txt_config_old)
        with open(self.program_logs, 'w') as f:
            f.write(self.logs_old)
        rmtree(self.trash_folder_location)
        rmtree(self.test_folder)

    def runTest(self):
        create_trash_folder(join(self.trash_folder_location, self.trash_folder_name))
        create_default_config_files(self.trash_folder_location, self.trash_folder_name)

        deleted_file = join(self.trash_folder_location, 'Trash', 'files', 'file')

        command = "python {} rm {}".format(self.program_script, self.file)
        self.assertEqual(subprocess.call(command, shell=True), 0)

        self.assertFalse(exists(join(self.file)))
        self.assertTrue(exists(deleted_file))

        command = "python {} rs {}".format(self.program_script, 'file')
        self.assertEqual(subprocess.call(command, shell=True), 0)

        self.assertTrue(exists(join(self.file)))
        self.assertFalse(exists(join(self.trash_folder_location, 'Trash', 'files', 'file')))

        command = "python {} show".format(self.program_script)
        self.assertEqual(subprocess.call(command, shell=True), 0)


def suite():
    result = unittest.TestSuite()
    result.addTest(ConsoleCommandsTestCase())
    return result
