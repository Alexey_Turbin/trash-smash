import unittest
from os import (
    mkfifo,
    mkdir,
    chmod,
    listdir,
)
from os.path import (
    dirname,
    abspath,
    join,
    exists,
    basename,
)
from shutil import rmtree

from trashsmash.modules import empty_module


class EmptyTrashTestCase(unittest.TestCase):
    trash_folder = join(dirname(abspath(__file__)), 'TestDir', 'trash')
    files_in_trash = join(trash_folder, 'files')
    first_file_name = 'file1'
    second_file_name = 'file2'

    def setUp(self):
        mkdir(self.trash_folder)
        mkdir(self.files_in_trash)
        mkfifo(join(self.files_in_trash, self.first_file_name))
        mkfifo(join(self.files_in_trash, self.second_file_name))

    def tearDown(self):
        rmtree(self.trash_folder)

    def runTest(self):
        list_of_files_in_trash = {
            self.first_file_name: 'file1',
            self.second_file_name: 'file2',
        }
        self.assertEqual(empty_module.empty_trash_folder(self.trash_folder, list_of_files_in_trash), 0)
        self.assertEqual(listdir(self.files_in_trash), [])


class CheckReadyTestCase(unittest.TestCase):
    trash_folder = join(dirname(abspath(__file__)), 'TestDir', 'trash')
    first_file_name = 'file1'
    second_file_name = 'file2'
    third_file_name = 'file3'
    list_of_files_in_trash = [
        first_file_name,
        second_file_name,
        third_file_name,
    ]

    def setUp(self):
        mkdir(self.trash_folder)
        text = "1"*1000000
        with open(join(self.trash_folder, self.first_file_name), 'w') as f:
            f.write(text)
        with open(join(self.trash_folder, self.second_file_name), 'w') as f:
            f.write(text)
        with open(join(self.trash_folder, self.third_file_name), 'w') as f:
            f.write(text)

    def tearDown(self):
        rmtree(self.trash_folder)

    def runTest(self):
        self.assertTrue(empty_module.check_ready(self.trash_folder, self.list_of_files_in_trash, max_weight=1))
        self.assertTrue(empty_module.check_ready(self.trash_folder, self.list_of_files_in_trash, max_count=2))
        self.assertFalse(empty_module.check_ready(self.trash_folder, self.list_of_files_in_trash, max_count=3))


def suite():
    result = unittest.TestSuite()
    result.addTest(EmptyTrashTestCase())
    result.addTest(CheckReadyTestCase())
    return result
