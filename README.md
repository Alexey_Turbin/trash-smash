#Trash-Smash 

##How to install trash-smash?
1. Download from repo.
2. Go to trash-smash-master folder ($cd .../trash-smash-master).
3. Write this command : $python setup.py install.
4. When you launch program first time(using command 'trashsmash' or 'smash'), 
program asks you to finish setup. Follow the instructions. 
 
##What can it do? 
1. Remove files ('trashsmash rm -h' for more information).
2. Restore files ('trashsmash rs -h' for more inormation).
3. Configure your bin ('trashsmash config -h' for more information).
4. Show deleted files ('trashsmash show -h' for more information).
5. Empty bin ('trashsmash empty -h' for more information).

##How to use?
1. Write '$trashsmash ...'.
2. Write '$smash ...' (use this command if you need 'rm' style).
