from os.path import (
    abspath,
    dirname,
    join,
)
from utils import (
    create_trash_folder,
    setup,
    ThrowingArgumentParser,
    ArgumentParserError,
)
from modules.remove_module import setup_argparser_for_short_command
import json
import modules as m
import logging
import logging.config
import argparse

__version__ = '0.1'


def _real_main(full_command=True):
    logging.debug("Program started.")

    configuration = load_config()
    setup_logger(configuration)
    modules = m.load_modules()

    if full_command:
        argparser = load_argparser(modules)
    else:
        argparser = load_argparser()
        setup_argparser_for_short_command(argparser)

    try:
        arguments = argparser.parse_args()
    except ArgumentParserError as e:
        arguments = None
        logging.info(argparser.format_usage())
        logging.error(e.message)
        logging.debug("Program finished(code=1).")
        return 1

    update_config(configuration, arguments)

    result = arguments.function(configuration, arguments)

    logging.debug("Program finished(code={}).".format(result))
    return result


def load_config():
    """This function loads config from file."""

    logging.debug("Loading config.")

    current_location = dirname(abspath(__file__))

    with open(join(current_location, 'config'), 'r') as f:
        config_list = json.loads(f.read())

    if config_list['list']['default']['trash_folder_path'] is None:
        logging.info("Finishing setup.")
        result = setup()
        logging.info("Setup finished!")
        logging.info("Program finished(code={}).".format(result))
        exit(result)

    config = config_list['list'][config_list['name']]
    config['sys_folder'] = current_location

    logging.debug("Config loaded.")
    return config


def load_argparser(modules=()):
    """This function creates argparser."""

    logging.debug("Loading argparser.")

    parser = ThrowingArgumentParser(argument_default=argparse)
    parser.add_argument('--silent', action='store_true', default=False, help='disable output')
    parser.add_argument('--dry-run', action='store_true', default=False, help='dry-run mode')

    if modules:
        subparsers = parser.add_subparsers()
        for item in modules:
            item.setup_argparser(subparsers)

    logging.debug("Argparser loaded.")
    return parser


def setup_logger(config, silent=False):
    """This function sets up logger."""

    logging.debug("Setting up logger.")

    if config['silent'] or silent:
        config['logger']['root']['handlers'] = ['file_handler']

    logging.getLogger(__name__)
    logging.config.dictConfig(config['logger'])

    logging.debug("Logger setup completed.")


def update_config(configuration, arguments):
    """This function updates config depends on arguments."""

    logging.debug("Updating config.")

    configuration['dry_run'] = configuration['dry_run'] or arguments.dry_run
    configuration['silent'] = configuration['silent'] or arguments.silent

    if arguments.silent:
        setup_logger(configuration, silent=True)

    logging.debug("Config updated.")


def main():
    _real_main()


def main_rm():
    _real_main(full_command=False)