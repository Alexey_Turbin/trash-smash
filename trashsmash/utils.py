import argparse
import json
import logging
import logging.config
from shutil import rmtree
from os.path import (
    dirname,
    join,
    isabs,
    abspath,
    expanduser,
    expandvars,
    exists,
)
from os import (
    makedirs,
    mkfifo,
)
from modules.config_module import (
    write_txt_config_to_file,
    write_json_config_to_file,
)


def get_default_config(trash_folder_path, trash_folder_name, json_config, txt_config, logger_file_path):
    """This function returns default config."""

    logging.debug("Default config creation started.")

    config = {
        'trash_folder_path': trash_folder_path,
        'trash_folder_name': trash_folder_name,
        'logs': logger_file_path,
        'json_config': json_config,
        'txt_config': txt_config,
        'silent': False,
        'dry_run': False,
        'empty_policy': {
            'max_weight': -1,
            'max_count': -1},
        'ask_confirmation': False,
        'logger': get_default_logger_config(logger_file_path),
    }

    logging.debug("Default config created.")
    return config


def get_default_logger_config(logger_file_path):
    """This function returns default logger config."""

    logging.debug("Default logging config creation started.")

    config = {
        'version': 1,
        'disable_existing_loggers': True,
        'formatters': {
            'default_formatter': {
                'format': '%(levelname)s - %(message)s'
            },
        },
        'handlers': {
            'stream_handler': {
                'class': 'logging.StreamHandler',
                'level': 'INFO',
                'formatter': 'default_formatter',
            },
            'file_handler': {
                'class': 'logging.FileHandler',
                'level': 'INFO',
                'filename': logger_file_path,
                'formatter': 'default_formatter',
                'mode': 'w',
            }
        },
        'root': {
            'handlers': ['stream_handler', 'file_handler'],
            'level': 'NOTSET',
        }
    }

    logging.debug("Default logging config created.")
    return config


def create_default_config_files(
        trash_folder_path=None,
        trash_folder_name=None,
        txt_config_path=join(dirname(abspath(__file__)), 'config.txt'),
        json_config_path=join(dirname(abspath(__file__)), 'config'),
        logger_file_path=join(dirname(abspath(__file__)), 'logs.txt')):
    """This function creates default config files."""

    logging.debug("Create default config file.")

    config = {
        'name': 'default',
        'list': {
            'default': get_default_config(trash_folder_path,
                                          trash_folder_name,
                                          json_config_path,
                                          txt_config_path,
                                          logger_file_path),
        }
    }

    write_json_config_to_file(json_config_path, config)
    write_txt_config_to_file(txt_config_path, config)

    logging.debug("Default config file created.")


def create_trash_folder(folder_path):
    """This function creates trash folder."""

    logging.info("Creating trash folder.")

    if exists(folder_path):
        raise OSError(17, "File {} is already exist".format(folder_path))

    makedirs(join(folder_path, 'files'))

    with open(join(folder_path, 'list'), 'w') as f:
        f.write(json.dumps({}))

    logging.info("Trash folder created.")


def remove_trash_folder(folder_path):
    logging.info("Removing trash folder.")

    rmtree(folder_path, ignore_errors=True)

    logging.info("Trash folder removed.")
    return 0


def setup():
    """This function finishes program setup by creating trash folder."""

    trash_folder_path = expanduser('~/.local/share')

    answer = raw_input("Hi! Trash-smash almost ready to go!\n"
                       "The last thing you need is trash folder.\n"
                       "Default path: {}.\n"
                       "Continue?[Y/n](n-change path for trash folder):".format(join(trash_folder_path,
                                                                                     'TrashSmash')))

    if answer.lower().startswith('n'):
        while True:
            trash_folder_path = expandvars(expanduser(raw_input("Enter path:")))

            if not isabs(trash_folder_path):
                print("Path '{}' is not absolute!".format(trash_folder_path))
                continue

            if exists(join(trash_folder_path, 'TrashFolder')):
                print("'{}' is already exist".format(join(trash_folder_path,
                                                          'TrashFolder')))

            if raw_input("Trash folder path: '{}'.\n"
                         "Continue?[Y/n](n-another path):".format(join(trash_folder_path,
                                                                       'TrashFolder'))
                         ).lower().startswith('y'):
                break

    create_trash_folder(join(trash_folder_path, 'TrashSmash'))

    create_default_config_files(trash_folder_path, 'TrashSmash')


class ArgumentParserError(Exception):
    pass


class ThrowingArgumentParser(argparse.ArgumentParser):
    def error(self, message):
        raise ArgumentParserError(message)