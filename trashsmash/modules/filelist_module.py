import curses
import json
from os.path import (
    expandvars,
    expanduser,
    join,
)
import logging


def setup_argparser(subparsers, *args, **kwargs):
    """This function sets up parser for filelist module."""

    parser = subparsers.add_parser('show', help='show files in trash')
    parser.add_argument('--full_info', action='store_true', default=False, help='show full info')
    parser.set_defaults(function=execute)


def execute(config, arguments, *args, **kwargs):
    return show(join(config['trash_folder_path'], config['trash_folder_name']),
                arguments.full_info,)


def show(trash_folder_path, full_info, silent=False):
    """This function reads info and calls show_files function."""

    if not silent:
        logging.debug("Executing filelist module.")

    info_path = expandvars(expanduser(join(trash_folder_path, 'list')))

    with open(info_path, 'r') as f:
        list_of_files_in_trash = json.loads(f.read())

    result = show_files(
        list_of_files_in_trash,
        full_info=full_info,
        silent=silent,
    )

    if not silent:
        logging.debug("Filelist module executed(code={}).".format(result))
    return result


def show_files(list_of_files_in_trash, full_info=False, silent=False):
    """This function shows files in trash folder."""

    if not silent:
        logging.debug("Showing files.")

    if full_info:
        result = print_full(list_of_files_in_trash, silent=silent)
    else:
        result = print_short(list_of_files_in_trash, silent=silent)

    if not silent:
        logging.debug("Files are showed.")
    return result


def print_short(list_of_files_in_trash, silent=False):
    """This function prints names of files in trash."""

    if silent:
        return 0

    logging.debug("Printing names.")

    try:
        width = curses.initscr().getmaxyx()[1]
        width -= 7
        curses.endwin()
    except curses.error as e:
        width = 5

    if not list_of_files_in_trash:
        return 0

    # This part of code is very bad, but I didn't have time to do better(
    list_of_names = list_of_files_in_trash.keys()
    list_of_names.sort()
    list_of_names = ["{}){}".format(i+1, list_of_names[i]) for i in range(list_of_names.__len__())]
    list_of_len = [x.__len__() + 2 for x in list_of_names]
    list_len = list_of_len.__len__()
    columns = 1
    str_len = 0
    while str_len <= width:
        str_len = 0
        columns = columns + 1
        l = 0
        for i in range(columns):
            r = l + list_len / columns
            if i < list_len % columns: r = r + 1
            try:
                str_len = str_len + max(list_of_len[l:r]) if l != r else str_len + list_of_len[l]
            except IndexError as e:
                str_len = width + 1
                break
            l = r

    l = 0
    columns = columns - 1
    list_of_columns = []
    for i in range(columns):
        r = l + list_len / columns
        if i < list_len % columns:
            r = r + 1
        max_len = max(list_of_len[l:r]) if l != r else list_of_len[l]
        lst = list_of_names[l:r] if l != r else [list_of_names[l]]
        lst = [str(x) + " " * (max_len - x.__len__()) for x in lst]
        if i > 0 and list_of_columns[i - 1].__len__() > lst.__len__():
            lst.append('')
        list_of_columns.append(lst)
        l = r

    for i in range(list_len / columns + (1 if list_len % columns > 0 else 0)):
        logging.info(''.join([x[i] for x in list_of_columns]))

    logging.debug("Names are printed.")
    return 0


def print_full(list_of_files_in_trash, silent=False):
    """This function prints full info about files in trash."""

    if silent:
        return 0

    logging.debug("Printing full info.")

    list_of_names = list_of_files_in_trash.keys()
    list_of_names.sort()

    for i in range(list_of_names.__len__()):
        logging.info("{}){}:{}, {}".format(
            i+1,
            list_of_names[i],
            list_of_files_in_trash[list_of_names[i]]['old_name'],
            list_of_files_in_trash[list_of_names[i]]['date']))

    logging.debug("Full info printed.")
    return 0


def read_list_of_files(trash_folder_path):
    with open(join(trash_folder_path, 'list'), 'r') as f:
        return json.loads(f.read())


def write_list_of_files(trash_folder_path, list_of_files):
    with open(join(trash_folder_path, 'list'), 'w') as f:
        f.write(json.dumps(list_of_files))
