from os.path import (
    join,
)

from copy import deepcopy
import curses
import json
import logging


def setup_argparser(subparsers, *args, **kwargs):
    """This function sets up parser for config module."""

    parser = subparsers.add_parser('config', help='program configuration settings')
    sub = parser.add_subparsers()

    parser_check = sub.add_parser('checkout', help='set current working config')
    parser_check.add_argument('config_to_checkout', help='config to checkout')
    parser_check.set_defaults(config_func=execute_checkout)

    parser_create = sub.add_parser('create', help='create new config')
    parser_create.add_argument('new_config_name', help='config name to create')
    parser_create.add_argument('--dry_run_default', type=bool, help='dry-run mode by default')
    parser_create.add_argument('--silent_default', type=bool, help='disable output by default')
    parser_create.add_argument('--confirm_default', type=bool, help='user need to confirm every action by default')
    parser_create.add_argument('--file_logger_level', choices=('notset, debug', 'info', 'warning', 'error', 'critical'), help='set file logger level')
    parser_create.add_argument('--stream_logger_level', choices=('notset, debug', 'info', 'warning', 'error', 'critical'), help='set stream logger level')
    parser_create.add_argument('--max_weight', type=int, help='max weight(MB) of trash(-1 to turn off max weight)')
    parser_create.add_argument('--max_count', type=int, help='max files in trash folder(-1 to turn off max count)')
    parser_create.set_defaults(config_func=execute_create)

    parser_delete = sub.add_parser('delete', help='delete config')
    parser_delete.add_argument('config_to_delete', help='config name to delete')
    parser_delete.set_defaults(config_func=execute_delete)

    parser_setup = sub.add_parser('setup', help='setup config')
    parser_setup.add_argument('config_to_setup', help='config name to setup')
    parser_setup.add_argument('--dry_run_default', type=bool, help='dry-run mode by default')
    parser_setup.add_argument('--silent_default', type=bool, help='disable output by default')
    parser_setup.add_argument('--confirm_default', type=bool, help='user need to confirm every action by default')
    parser_setup.add_argument('--file_logger_level', choices=('notset, debug', 'info', 'warning', 'error', 'critical'), help='set file logger level')
    parser_setup.add_argument('--stream_logger_level', choices=('notset, debug', 'info', 'warning', 'error', 'critical'), help='set stream logger level')
    parser_setup.add_argument('--max_weight', type=int, help='max weight(MB) of trash(-1 to turn off max weight)')
    parser_setup.add_argument('--max_count', type=int, help='max files in trash folder(-1 to turn off max count)')
    parser_setup.set_defaults(config_func=execute_setup)

    parser_show = sub.add_parser('show', help='show info about configs')
    parser_show.add_argument('--full_configs_info', action='store_true', default=False, help='show full info')
    parser_show.set_defaults(config_func=execute_show)

    parser.set_defaults(function=execute)


def execute(config, arguments, *args, **kwargs):
    """This function reads config info, executes config module functions,
     and writes new info."""

    logging.info("Executing config module.")

    json_config_path = config['json_config']
    txt_config_path = config['txt_config']
    config_list = read_config_from_file(json_config_path)

    result = arguments.config_func(config_list, arguments)

    if not config['dry_run']:
        write_json_config_to_file(json_config_path, config_list)
        write_txt_config_to_file(txt_config_path, config_list)

    logging.info("Config module executed.")
    return result


def execute_checkout(config_list, arguments):
    """This function executes checkout_config function."""

    return checkout_config(config_list, arguments.config_to_checkout)


def execute_create(config_list, arguments):
    """This function executes create_config function"""

    return create_config(config_list,
                         arguments.new_config_name,
                         silent_default=arguments.silent_default,
                         dry_run_default=arguments.dry_run_default,
                         empty_max_weight=arguments.max_weight,
                         empty_max_count=arguments.max_count,
                         ask_confirmation=arguments.confirm_default,
                         file_logger_level=arguments.file_logger_level,
                         stream_logger_level=arguments.stream_logger_level)


def execute_delete(config_list, arguments):
    """This function executes delete_config function."""

    return delete_config(config_list, arguments.config_to_delete)


def execute_setup(config_list, arguments):
    """This function executes setup_config function."""

    return setup_config(config_list,
                        arguments.config_to_setup,
                        silent_default=arguments.silent_default,
                        dry_run_default=arguments.dry_run_default,
                        empty_max_weight=arguments.max_weight,
                        empty_max_count=arguments.max_count,
                        ask_confirmation=arguments.confirm_default,
                        file_logger_level=arguments.file_logger_level,
                        stream_logger_level=arguments.stream_logger_level)


def execute_show(config_list, arguments):
    """This function executes show_config function."""

    return show_configs(config_list, arguments.full_configs_info)


def show_configs(config_list, full_info):
    """This function shows configs."""

    logging.debug("Showing config.")

    try:
        line = '-'*curses.initscr().getmaxyx()[1]
        curses.endwin()
    except curses.error as e:
        line = '-'*10

    logging.info("Current:{}".format(config_list['name']))
    if full_info:
        for item in config_list['list']:
            config = config_list['list'][item]
            logging.info(line)
            logging.info("{}:".format(item))
            logging.info("TRASH FOLDER PATH={}".format(config['trash_folder_path']))
            logging.info("TRASH FOLDER NAME={}".format(config['trash_folder_name']))
            logging.info("SILENT={}".format(config['silent']))
            logging.info("DRY-RUN={}".format(config['dry_run']))
            logging.info("MAX WEIGHT={}".format(config['empty_policy']['max_weight']))
            logging.info("MAX COUNT={}".format(config['empty_policy']['max_count']))
            logging.info("CONFIRM={}".format(config['ask_confirmation']))
            logging.info("STREAM LOGGER LEVEL={}".format(config['logger']['handlers']['file_handler']['level']))
            logging.info("FILE LOGGER LEVEL={}".format(config['logger']['handlers']['stream_handler']['level']))

        logging.info(line)
    else:
        for item in config_list['list'].keys():
            logging.info(item)

    logging.debug("Config showing completed.")
    return 0


def setup_config(config_list, config_name, **kwargs):
    """This function sets up config.
        returns:
            0 - everything is ok
            1 - wrong config name
            2 - can't change default config
        """

    logging.debug("Setting up config.")

    if config_name == 'default':
        logging.error("Can't change default config.")
        return 2
    if config_name not in config_list['list']:
        logging.error("Config '{}' not found.".format(config_name))
        return 1

    config_list['list'][config_name] = setup_config_params(config_list['list'][config_name], **kwargs)

    logging.info("Config '{}': setup completed.".format(config_name))
    return 0


def delete_config(config_list, config_name):
    """This function deletes config.
    returns:
        0 - everything is ok
        1 - wrong config name
        2 - can't remove default config
    """

    logging.debug("Deleting config.")

    if config_name == 'default':
        logging.error("Can't remove default config.")
        return 2
    if config_name not in config_list['list']:
        logging.error("Config '{}' not found.".format(config_name))
        return 1

    config_list['list'].pop(config_name)

    if config_name == config_list['name']:
        checkout_config(config_list, 'default')

    logging.info("Config '{}' deleted.".format(config_name))
    return 0


def checkout_config(config_list, config_name):
    """This function changes current config."""

    logging.debug("Switching config.")

    if config_name not in config_list['list']:
        logging.error("Config '{}' not found.".format(config_name))
        return 1

    config_list['name'] = config_name

    logging.info("Switched to '{}' config.".format(config_name))
    return 0


def create_config(config_list, config_name, **kwargs):
    """This function creates new config based on default and adds it to config list."""

    logging.debug("Creating new config.")

    if config_name in config_list['list']:
        logging.error("Config '{}' is already exists.".format(config_name))
        return 1

    config_list['list'][config_name] = setup_config_params(
        deepcopy(config_list['list']['default']),
        **kwargs
    )

    logging.info("Config '{}' created.".format(config_name))
    return 0


def read_config_from_file(path):
    """This function reads config from file."""

    with open(path, 'r') as f:
        return json.loads(f.read())


def write_json_config_to_file(path, config_list):
    """This function writes json config to file."""

    with open(path, 'w') as f:
        f.write(json.dumps(config_list))


def write_txt_config_to_file(path, config_list):
    """This function writes current config to txt file."""

    config = config_list['list'][config_list['name']]

    with open(path, 'w') as f:
        f.write("TRASH FOLDER PATH={}\n".format(config['trash_folder_path']))
        f.write("TRASH FOLDER NAME={}\n".format(config['trash_folder_name']))
        f.write("JSON CONFIG={}\n".format(config['json_config']))
        f.write("TXT CONFIG={}\n".format(config['txt_config']))
        f.write("LOGS={}\n".format(config['logs']))
        f.write("SILENT BY DEFAULT={}\n".format(config['silent']))
        f.write("DRY-RUN BY DEFAULT={}\n".format(config['dry_run']))
        f.write("MAX WEIGHT={}\n".format(config['empty_policy']['max_weight']))
        f.write("MAX COUNT={}\n".format(config['empty_policy']['max_count']))
        f.write("ASK CONFIRMATION BY DEFAULT={}\n".format(config['ask_confirmation']))
        f.write("STREAM LOGGER LEVEL={}\n".format(config['logger']['handlers']['stream_handler']['level']))
        f.write("FILE LOGGER LEVEL={}\n".format(config['logger']['handlers']['file_handler']['level']))


def setup_config_params(config,
                        trash_folder_path=None,
                        trash_folder_name=None,
                        silent_default=None,
                        dry_run_default=None,
                        empty_max_weight=None,
                        empty_max_count=None,
                        ask_confirmation=None,
                        file_logger_level=None,
                        stream_logger_level=None):
    """This function changes config parameters."""

    logging.debug("Setting up config parameters.")

    if trash_folder_path is not None:
        config['trash_folder_path'] = trash_folder_path

    if trash_folder_name is not None:
        config['trash_folder_name'] = trash_folder_name

    if silent_default is not None:
        config['silent'] = silent_default

    if dry_run_default is not None:
        config['dry_run'] = dry_run_default

    if empty_max_weight is not None:
        config['empty_policy']['max_weight'] = empty_max_weight

    if empty_max_count is not None:
        config['empty_policy']['max_count'] = empty_max_count

    if ask_confirmation is not None:
        config['ask_confirmation'] = ask_confirmation

    if file_logger_level is not None:
        config['logger']['handlers']['file_handler']['level'] = file_logger_level.upper()

    if stream_logger_level is not None:
        config['logger']['handlers']['stream_handler']['level'] = stream_logger_level.upper()

    logging.debug("Setting up config parameters completed.")
    return config
