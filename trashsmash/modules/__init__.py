import logging


def load_modules():
    logging.debug("Loading modules.")
    result = []

    try:
        import remove_module
        result.append(remove_module)
    except ImportError as e:
        logging.error("Remove module doesn't work.")
        remove_module = None

    try:
        import restore_module
        result.append(restore_module)
    except ImportError as e:
        logging.error("Restore module doesn't work.")
        restore_module = None

    try:
        import filelist_module
        result.append(filelist_module)
    except ImportError as e:
        logging.error("Filelist module doesn't work.")
        filelist_module = None

    try:
        import config_module
        result.append(config_module)
    except ImportError as e:
        logging.error("Config module doesn't work.")
        config_module = None

    try:
        import empty_module
        result.append(empty_module)
    except ImportError as e:
        logging.error("Empty module doesn't work.")
        empty_module = None

    logging.debug("Modules loaded.")
    return result
