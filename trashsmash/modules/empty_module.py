import json
import logging
from os import (
    remove,
    walk,
)
from os.path import (
    getsize,
    join,
    expanduser,
    expandvars,
    isdir,
)
from shutil import rmtree


def setup_argparser(subparsers, *args, **kwargs):
    """This function sets up parser for empty module."""

    parser = subparsers.add_parser('empty', help='empty trash folder.')
    parser.set_defaults(function=execute)


def execute(config, arguments, *args, **kwargs):
    return empty(join(config['trash_folder_path'], config['trash_folder_name']),
                 dry_run=config['dry_run'])


def empty(trash_folder_path, dry_run=False, silent=False):
    """This function reads info, execute empty_trash_folder function, and writes new info."""

    if not silent:
        logging.debug("Executing empty module.")

    dry_run = dry_run
    trash_folder_real_path = expandvars(expanduser(trash_folder_path))
    info_file = join(trash_folder_real_path, 'list')

    with open(info_file, 'r') as f:
        list_of_files_in_trash = json.loads(f.read())

    result = empty_trash_folder(trash_folder_real_path,
                                list_of_files_in_trash,
                                dry_run=dry_run,
                                silent=silent)

    if not dry_run:
        with open(info_file, 'w') as f:
            f.write(json.dumps(list_of_files_in_trash))

    if not silent:
        logging.debug("Empty module executed(code={}).".format(result))
    return result


def empty_trash_folder(trash_folder_path,
                       list_of_files_in_trash,
                       dry_run=False,
                       silent=False):
    """This function delete all files in trash list.
        returns:
            0 - all files deleted
            1 - trash info has file but trash folder doesn't
    """

    if not silent:
        logging.debug("Emptying started.")
    result = 0

    for file_name in list_of_files_in_trash.keys():
        file_path = join(trash_folder_path, 'files', file_name)

        try:
            if not dry_run:
                if isdir(file_path):
                    rmtree(file_path)
                else:
                    remove(file_path)
            list_of_files_in_trash.pop(file_name)

        except OSError as e:
            if e.errno == 2:
                if not silent:
                    logging.error("Can't remove '{}': {}".format(file_name, e.strerror))
                result = 1
            else:
                raise

    if not silent:
        logging.debug("Emptying completed(code={}).".format(result))
    return result


def check_ready(trash_folder_path, list_of_files_in_trash, max_weight=-1, max_count=-1):
    """This function checks if trash folder ready to empty."""

    if 0 <= max_count < list_of_files_in_trash.__len__():
        return True
    elif 0 <= max_weight < get_trash_folder_size(trash_folder_path, list_of_files_in_trash):
        return True

    return False


def get_trash_folder_size(trash_folder_path, list_of_files_in_trash):
        """This function returns size of all elements in trash info(mb)."""

        size = 0
        folder = join(trash_folder_path)

        for item in list_of_files_in_trash:
            item_path = join(folder, item)
            if isdir(item_path):
                size += get_folder_size(item_path)
            else:
                size += getsize(item_path)

        return size / 1000000


def get_folder_size(start_path):
    """This folder returns folder size(bytes)."""

    total_size = 0

    for dirpath, dirnames, filenames in walk(start_path):
        for f in filenames:
            try:
                fp = join(dirpath, f)
                total_size += getsize(fp)
            except OSError as e:
                total_size += 10

    return total_size
