import logging
import json
from os import (
    rename,
    makedirs
)
from os.path import (
    join,
    exists,
    dirname,
    expanduser,
    expandvars,
)


def setup_argparser(subparsers, *args, **kwargs):
    """This function adds parser for restore module"""

    parser = subparsers.add_parser('rs', help='restore files')
    parser.add_argument('files_to_restore', nargs='+', help='files to remove')
    parser.add_argument('--ask-conf', action='store_true', default=False, help='ask confirmation before acton')
    parser.add_argument('--short-name', action='store_false', default=True, help='use short name to restore')
    parser.set_defaults(function=execute)


def execute(config, arguments, *args, **kwargs):
    trash_folder = join(config['trash_folder_path'], config['trash_folder_name'])
    return restore(arguments.files_to_restore,
                   trash_folder,
                   dry_run=config['dry_run'],
                   ask_confirmation=config['ask_confirmation'] or arguments.ask_conf,
                   full_name=arguments.short_name)


def restore(files_to_restore,
            move_from,
            dry_run=False,
            ask_confirmation=False,
            full_name=True,
            silent=False):
    """This function reads trash info, executes restore_files function, and saves new info."""

    if not silent:
        logging.debug("Executing restore module.")

    trash_path = expandvars(expanduser(move_from))
    info_path = join(trash_path, 'list')

    with open(info_path, 'r') as f:
        list_of_files_in_trash = json.loads(f.read())

    result = restore_files(files_to_restore,
                           join(trash_path, 'files'),
                           list_of_files_in_trash,
                           dry_run=dry_run,
                           ask_confirmation=ask_confirmation,
                           full_name=full_name,
                           silent=silent)

    if not dry_run:
        with open(info_path, 'w') as f:
            f.write(json.dumps(list_of_files_in_trash))

    if not silent:
        logging.debug("Restore module executed(code={}).".format(result))
    return result


def restore_files(files_to_restore,
                  move_from,
                  list_of_files_in_trash,
                  dry_run=False,
                  ask_confirmation=False,
                  full_name=True,
                  silent=False):
    """This function restores files using _restore_file function
        returns:
            0 - all files were restored
            1 - several errors
            2 - trash info has file , but trash folder doesn't
            3 - permission denied
            4 - file with the same name is already exist
            5 - can't create old directory
            6 - no such file in trash list
            7 - can't choose file to restore(names of two or more files start same)
    """

    if not silent:
        logging.debug("Restoring started.")
    result = 0

    for file_to_restore in files_to_restore:

        if ask_confirmation:
            if not silent:
                logging.warn("Do you want to restore '{}'?".format(file_to_restore))
            if not raw_input().lower().startswith('y'):
                continue

        if not full_name and file_to_restore not in list_of_files_in_trash:
            same_name = list(filter(lambda x: x.startswith(file_to_restore),
                                    list_of_files_in_trash))
            if same_name.__len__() == 1:
                file_to_restore = same_name[0]
            elif same_name.__len__() > 1:
                if not silent:
                    logging.error("Can't choose file to restore.")
                    logging.info("Choose:")
                for f in same_name:
                    print f
                result = 7 if result == 0 else 1
                continue

        try:
            restore_file(file_to_restore,
                         move_from,
                         list_of_files_in_trash,
                         dry_run=dry_run,
                         silent=silent)

        except OSError as e:
            if not silent:
                logging.error("Can't restore file '{}': {}.".format(file_to_restore,
                                                                    e.strerror))

            if e.errno == 2:
                list_of_files_in_trash.pop(file_to_restore)
                result = 2 if result == 0 else 1
            elif e.errno == 13:
                result = 3 if result == 0 else 1
            elif e.errno == 17:
                result = 4 if result == 0 else 1
            elif e.errno == 5:
                result = 5 if result == 0 else 1
            else:
                raise
        except KeyError as e:
            if not silent:
                logging.error("Can't restore file '{}': No such file in trash list.".format(file_to_restore))
            result = 6 if result == 0 else 1

    if not silent:
        logging.debug("Restoring completed(code={}).".format(result))
    return result


def restore_file(file_to_restore,
                 move_from,
                 list_of_files_in_trash,
                 dry_run=False,
                 silent=False):
    """This function restores file and removes info about it"""

    if not silent:
        logging.debug("Restoring '{}'.".format(file_to_restore))

    if exists(list_of_files_in_trash[file_to_restore]['old_name']):
        raise OSError(17, "File with the same name is already exist")

    if not exists(dirname(list_of_files_in_trash[file_to_restore]['old_name'])):
        try:
            makedirs(dirname(list_of_files_in_trash[file_to_restore]['old_name']))
        except OSError as e:
            raise OSError(5, "Can't create old directory '{}':{}".format(e.filename, e.strerror))

    rename(join(move_from, file_to_restore), list_of_files_in_trash[file_to_restore]['old_name'])

    if dry_run:
        rename(list_of_files_in_trash[file_to_restore]['old_name'], join(move_from, file_to_restore))

    list_of_files_in_trash.pop(file_to_restore)

    if not silent:
        logging.debug("'{}' restored.".format(file_to_restore))
    return 0
