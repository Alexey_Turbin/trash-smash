import re
import json
import datetime
import logging
import uuid
from multiprocessing.dummy import Pool
from os import (
    rename,
    listdir,
)
from os.path import (
    join,
    basename,
    isdir,
    dirname,
    exists,
    expanduser,
    expandvars,
    abspath,
)


def setup_argparser(subparsers, *args, **kwargs):
    """This function adds parser for remove module."""

    parser = subparsers.add_parser('rm', help='remove files')
    parser.add_argument('files_to_remove', nargs='+', help='files to remove')
    parser.add_argument('--ask-conf', action='store_true', default=False, help='ask confirmation before action')
    parser.add_argument('--hash', action='store_true', default=False, help='use hash for files name')
    parser.add_argument('--regex', nargs=1, default=False, help='remove all files matching regular expression')
    parser.set_defaults(function=execute)


def setup_argparser_for_short_command(parser, *args, **kwargs):
    """This function sets up argparser for short command."""

    parser.add_argument('files_to_remove', nargs='+', help='files to remove')
    parser.add_argument('--ask-conf', action='store_true', default=False, help='ask confirmation before action')
    parser.add_argument('--hash', action='store_true', default=False, help='use hash for files name')
    parser.add_argument('--regex', nargs=1, default=False, help='remove all files matching regular expression')
    parser.set_defaults(function=execute)


def execute(config, arguments, *args, **kwargs):
    trash_folder = join(config['trash_folder_path'], config['trash_folder_name'])
    if arguments.regex:
        regex = arguments.regex
    else:
        regex = None

    return remove(arguments.files_to_remove,
                  trash_folder,
                  sys_folders=[config['sys_folder'], trash_folder],
                  dry_run=config['dry_run'],
                  ask_confirmation=config['ask_confirmation'] or arguments.ask_conf,
                  max_weight=config['empty_policy']['max_weight'],
                  max_count=config['empty_policy']['max_count'],
                  hash_name=arguments.hash,
                  regex=regex)


def remove(files_to_remove,
           trash_path,
           sys_folders=(),
           dry_run=False,
           ask_confirmation=False,
           max_weight=-1,
           max_count=-1,
           hash_name=False,
           silent=False,
           regex=None):
    """This function reads trash info, executes remove_files function,
        empty trash folder if needed and saves new info."""

    if not silent:
        logging.debug("Executing remove module.")

    trash_real_path = expandvars(expanduser(trash_path))
    info_path = join(trash_real_path, 'list')

    with open(info_path, 'r') as f:
        list_of_files_in_trash = json.loads(f.read())

    if regex:
        files_to_remove = change_paths_by_regex_paths(files_to_remove, regex[0])

    result = remove_files(files_to_remove,
                          join(trash_real_path, 'files'),
                          list_of_files_in_trash,
                          sys_folders=sys_folders,
                          dry_run=dry_run,
                          ask_confirmation=ask_confirmation,
                          hash_name=hash_name,
                          silent=silent)

    try:
        import empty_module
        if empty_module.check_ready(join(trash_real_path, 'files'),
                                    list_of_files_in_trash,
                                    max_weight,
                                    max_count):
            empty_module.empty_trash_folder(trash_real_path,
                                            list_of_files_in_trash,
                                            dry_run=dry_run)
            if not silent:
                logging.info("Trash folder is empty.")

    except ImportError as e:
        empty_module = None
        if not silent:
            logging.error("Empty module doesn't work: {}.".format(e.message))

    if not dry_run:
        with open(info_path, 'w') as f:
            f.write(json.dumps(list_of_files_in_trash))

    if not silent:
        logging.debug("Remove module executed(code={}).".format(result))
    return result


def remove_files(files_to_remove,
                 move_where,
                 list_of_files_in_trash=None,
                 dry_run=False,
                 sys_folders=(),
                 ask_confirmation=False,
                 hash_name=False,
                 silent=False):
    """This function removes files using _remove_file function.
            returns:
                0 - all files were removed
                1 - several errors
                2 - no such file or directory
                3 - permission denied
                4 - can't remove system files or folders
        """

    if not silent:
        logging.debug("Removing files.")
    result = 0

    for file_name in files_to_remove:
        real_file_path = abspath(expandvars(expanduser(file_name)))

        if ask_confirmation:
            if not silent:
                logging.warn("Do you want to delete '{}'?".format(real_file_path))
            if not raw_input().lower().startswith('y'):
                continue

        function_result = remove_file(real_file_path,
                                      move_where,
                                      list_of_files_in_trash,
                                      sys_folders=sys_folders,
                                      dry_run=dry_run,
                                      hash_name=hash_name,
                                      silent=silent)

        if result == 0:
            result = function_result
        else:
            result = 1

    if not silent:
        logging.debug("Removing completed(code={}).".format(result))
    return result


def remove_file(file_to_remove,
                move_where,
                list_of_files_in_trash=None,
                sys_folders=(),
                dry_run=False,
                hash_name=False,
                silent=False):
    """This function moves file to trash folder and adds info about it.
    Changes file name if file with the same name is already exists in trash folder.
            returns:
                0 - all files were removed
                1 - several errors
                2 - no such file or directory
                3 - permission denied
                4 - can't remove system files or folders
    """

    if not silent:
        logging.debug("Moving '{}' to trash folder.".format(file_to_remove))

    try:
        for sys_folder in sys_folders:
            if file_to_remove.startswith(sys_folder):
                raise OSError(22, "Can't remove system files or folders")

        new_path = set_new_path(join(move_where, basename(file_to_remove)),
                                hash_name=hash_name)

        rename(file_to_remove, new_path)

        if dry_run:
            rename(new_path, file_to_remove)

        if list_of_files_in_trash is None:
            list_of_files_in_trash = {}

        list_of_files_in_trash[str(basename(new_path))] = {
            'old_name': str(file_to_remove),
            'date': str(datetime.datetime.now()),
        }

    except OSError as e:
        if not silent:
            logging.error("Can't remove file '{}': {}.".format(file_to_remove, e.strerror))
        if e.errno == 2:
            return 2
        elif e.errno == 13:
            return 3
        elif e.errno == 22:
            return 4
        else:
            raise

    if not silent:
        logging.debug("'{}' moved to trash folder.".format(file_to_remove))
    return 0


def set_new_path(path, hash_name=False):
    """Changes file name if file with the same name is already exist in trash folder."""

    real_path = expandvars(expanduser(path))

    if hash_name:
        real_path = join(dirname(real_path), str(uuid.uuid4()))
    elif exists(real_path):
        dir_path = dirname(real_path)
        split_name = basename(real_path).split('.', 1)
        first = split_name[0]
        second = "" if split_name.__len__() == 1 else "." + str(split_name[1])
        numb = 1
        while exists(join(dir_path, "{}.{}{}".format(first, numb, second))):
            numb += 1
        real_path = join(dir_path, "{}.{}{}".format(first, numb, second))

    return real_path


def get_path_by_regex(path, regex):
    names = list(filter(lambda x: re.match(regex, x), listdir(path)))
    return [join(path, x) for x in names]


def change_paths_by_regex_paths(paths, regex):
    real_paths = [expandvars(expanduser(x)) for x in paths]
    result = []

    for path in real_paths:
        result.extend(get_path_by_regex(path, regex))

    return result


def remove_multiproc(files_to_remove,
                     trash_path,
                     sys_folders=(),
                     dry_run=False):
    """This function removes all files matching regular expression,
    uses several processes"""

    if not files_to_remove:
        return {}

    files_real_path = [expandvars(expanduser(x)) for x in files_to_remove]
    results = {x: {} for x in files_real_path}

    pool = Pool(processes=files_real_path.__len__())

    pool_results = pool.map(lambda x: remove_file(x,
                                                  join(trash_path, 'files'),
                                                  list_of_files_in_trash=results[x],
                                                  sys_folders=sys_folders,
                                                  dry_run=dry_run,
                                                  silent=True,
                                                  hash_name=True),
                            files_real_path)
    pool.close()
    pool.join()

    for i in range(files_real_path.__len__()):
        if results[files_to_remove[i]]:
            new_name = results[files_to_remove[i]].keys()[0]
            new_path = join(trash_path, 'files', new_name)
            results[files_to_remove[i]]['new_path'] = new_path
            results[files_to_remove[i]].pop(new_name)
        else:
            results[files_to_remove[i]]['new_path'] = None

        results[files_to_remove[i]]['code'] = pool_results[i]

    return results


def remove_regex(dir_path,
                 regex,
                 trash_path,
                 sys_folders=(),
                 dry_run=False,
                 is_recursive=False):
    """This function removes all files recursive using regular expression"""

    real_path = expandvars(expanduser(dir_path))

    try:
        files_to_remove = change_paths_by_regex_paths((real_path,), regex)
    except OSError as e:
        if e.errno == 13:
            return {}
        else:
            raise

    result = {}

    try:
        if is_recursive:
            new_paths = [join(real_path, x) for x in list(filter(lambda y: isdir(join(real_path, y)),
                                                                 listdir(dir_path)))]
        else:
            new_paths = ()
    except OSError as e:
        new_paths = ()

    for new_path in new_paths:
        res = remove_regex(new_path,
                           regex,
                           trash_path,
                           sys_folders=sys_folders,
                           is_recursive=is_recursive)

        for item in res:
            result[item] = res[item]

    remove_result = remove_multiproc(files_to_remove, trash_path, sys_folders=sys_folders, dry_run=dry_run)
    for item in remove_result:
        result[item] = remove_result[item]

    return result














