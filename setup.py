from setuptools import setup, find_packages
from os.path import (
    join,
    dirname,
)

setup(
    name='trashsmash',
    version='1.0',
    packages=find_packages(),
    long_description=open(join(dirname(__file__), 'README.md')).read(),
    entry_points={
        'console_scripts': [
            'trashsmash = trashsmash:main',
            'smash = trashsmash:main_rm',
        ],
    },
    package_data={'': ['config', 'config.txt', 'logs.txt']},
    test_suite='tests',
)
